from userAuthentication import db


class User(db.Model):

	def __repr__(self):
		return f"{self.user_name} with the user id {self.id}"

	id = db.Column(db.Integer, primary_key=True)
	first_name = db.Column(db.String(50))
	last_name = db.Column(db.String(50))
	user_name = db.Column(db.String(50), unique=True)
	password = db.Column(db.String(300))
	# first open the image with mathplotlib.image.imread that returns numpy nd-array and then change it to string with numpy.array_str to avoid any complications
	# find out how they can parse that image string and change convert to numpy array
	# TODO: find the better way to handle pictures
	pic = db.Column(db.String(2000))