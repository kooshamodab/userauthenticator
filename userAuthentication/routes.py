from userAuthentication import app, db
from werkzeug.security import generate_password_hash, check_password_hash
from userAuthentication.models import User
from flask import request, jsonify
from functools import wraps
import jwt


def token_required(f):
	@wraps(f)
	def inner(*args,**kwargs):

		token = request.headers.get('x-access-token')

		if not token:
			return jsonify({'message' : 'token is missing'})

		try:
			data = jwt.decode(token,algorithms='HS256',options={"verify_signature": False})

			current_user = User.query.filter_by(id=data['id'])
		except:
			return jsonify({'message' : 'invalid token'})

		return f(current_user,*args,**kwargs)

	return inner



@app.route('/write' ,methods=['POST'])
@token_required
def create_user(current_user):
	data = request.get_json()
	hashed_pass = generate_password_hash(data['password'])

	new_user = User(first_name=data.get('first_name'),last_name=data.get('last_name')
					,user_name=data['user_name']
					,password=hashed_pass,
					pic=data.get('picture'))
	db.session.add(new_user)
	db.session.commit()

	return jsonify({'message':'new user created'})




@app.route('/get_profile' ,methods=['GET'])
def get_profile():

	user_name = request.args.get('user_name')
	if user_name == None:
		return jsonify({'Message':'Didnt get username'})
	user = User.query.filter_by(user_name=user_name).first()


	return jsonify({'user_name':user.user_name,
				'first_name':user.first_name,
				'last_name':user.last_name,
				'pic':user.pic})


@app.route('/get_all')
@token_required
def get_all(current_user):
	users = list(User.query.all())
	out=[]
	for user in users:
		out.append({'name':user.user_name,
					'pic':user.pic})
	return jsonify({'users':out})



@app.route('/login')
def login():
	auth = request.authorization

	if not auth or not auth.username or not auth.password:
		return jsonify({'message':'credentials not found'})

	user = User.query.filter_by(user_name=auth.username).first()

	if not user:
		return jsonify({'message':'user not found!'})

	if check_password_hash(user.password,auth.password):
		token = jwt.encode({"id":user.id},app.config['SECRET_KEY'])
		return jsonify({'token':token})



	return jsonify({'message':'invalid credentials'})




